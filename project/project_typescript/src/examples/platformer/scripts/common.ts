
//runs on collisions events
export function playerGroundCheck(event, ground, playerSensor, player): Matter.IBodyDefinition {
  let pairs = event.pairs;
  for (let i = 0, j = pairs.length; i != j; ++i) {
    let pair = pairs[i];
    if (pair.bodyA === playerSensor) {
      player.ground = ground;
    } else if (pair.bodyB === playerSensor) {
      player.ground = ground;
    }
  }
  return player;
}
